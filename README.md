MSAViewer Extension Module
==========================

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Use
 * Maintainers


INTRODUCTION
------------
This extension provides a simple file formatter for genomic sequence alignements
files and a text filter using the library [1][MSAViewer] for display.

Related paper to cite:
Yachdav, Guy, Sebastian Wilzbach, Benedikt Rauscher, Robert Sheridan, Ian
Sillitoe, James Procter, Suzanna E. Lewis, Burkhard Rost, and Tatyana Goldberg.
"MSAViewer: interactive JavaScript visualization of multiple sequence
alignments." Bioinformatics (2016): btw474.

[1]: https://msa.biojs.net/


REQUIREMENTS
------------

This module requires the following:

 * Libraries API (https://www.drupal.org/project/libraries)
 * MSAViewer library



INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 * Install the MSAViewer library:
   download https://cdn.bio.sh/msa/1.0/msa.min.gz.js
   and save it to a msaviewer directory in your library directory (so
   you should have: .../sites/XXX/libraries/msaviewer/msa.min.gz.js).
   Then unzip it to have an "msa.js" file:
   `gunzip msa.min.gz.js -c > msa.js`

 * Enable the module in "Admin menu > Site building > Modules" (path
   /admin/modules).


USE
---

Example 1:

 * Create or use an existing content type.

 * Add a "file" field to that content type (admin/structure/types --> manage
   fields).

 * Change the formatter of that file (manage display) to use "MSAViewer".

 * Create a corresponding content with an alignment file (FASTA, Clustal,...)
   and display it.

note: this formatter can also be used in views or other places and it supports
multiple trees on a same page.

Example 2:
  * Edit a text format (admin/config/content/formats) and check the filter
    "MSAViewer Filter".

  * In the "Filter processing order" section, make sure the filter "MSAViewer
    Filter" comes before other filters which may alter HTML tags and save.

  * Create a new content that have a (multiple lines) text field.

  * Add a tag <msaviewer url="*[URL to an alignment file]*"> in the text field.

  * Select the text format that uses the "MSAViewer" filter and save.


MAINTAINERS
-----------

Current maintainer:

 * Valentin Guignon (vguignon) - https://www.drupal.org/user/423148
